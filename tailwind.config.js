/** @type {import('tailwindcss').Config} */
export default {
	content: ["./src/**/*.{js,jsx,ts,tsx}"],
	theme: {
		extend: {
			fontFamily: {
				poppins: ["Poppins"],
			},
			spacing: {
				600: "600px",
			},
		},
	},
	plugins: [],
};
