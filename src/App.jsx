import { useState } from "react";
import { Tab } from "@headlessui/react";
import Form from "./Form";
import Form2 from "./Form2";
import Card from "./Card";

function classNames(...classes) {
	return classes.filter(Boolean).join(" ");
}

function Main() {
	let [categories] = useState({
		step1: <Form />,
		step2: <Form2 />,
	});

	return (
		<div className="flex justify-center items-center flex-col">
			<div className="w-full max-w-md px-2 py-16 sm:px-0">
				<Tab.Group>
					<Tab.List className="flex space-x-1 rounded-xl bg-blue-900/20 p-1">
						{Object.keys(categories).map((category) => (
							<Tab
								key={category}
								className={({ selected }) =>
									classNames(
										"w-full rounded-lg py-2.5 text-sm font-medium leading-5 text-blue-700",
										"ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2",
										selected ? "bg-white shadow" : "text-blue-100 hover:bg-white/[0.12] hover:text-white"
									)
								}
							>
								{category}
							</Tab>
						))}
					</Tab.List>
					<Tab.Panels className="mt-2">
						{Object.values(categories).map((component, idx) => (
							<Tab.Panel
								key={idx}
								className={classNames("rounded-xl bg-white p-3", "ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2")}
							>
								{component}
							</Tab.Panel>
						))}
					</Tab.Panels>
				</Tab.Group>
			</div>
			<Card />
		</div>
	);
}

export default function App() {
	return (
		<div className="flex justify-center bg-gray-100">
			<Main />
		</div>
	);
}
