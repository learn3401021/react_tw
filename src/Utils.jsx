function TextField({ label, name, value, onChange, required = false, placeholder }) {
	return (
		<div className="mb-4 size-14 leading-20">
			<label
				htmlFor={name}
				className="block mb-1 weight-500 font-14"
			>
				{label}
				{required && <span className="text-red-500">*</span>}
			</label>
			<input
				type="text"
				id={name}
				name={name}
				className="w-full border border-gray-300 rounded px-3 py-2"
				value={value}
				onChange={onChange}
				placeholder={placeholder}
				required={required}
			/>
		</div>
	);
}
const RadioField = ({ label, name, value, checked, onChange }) => {
	return (
		<div className="flex items-center mr-4 size-14 leading-20">
			<input
				type="radio"
				id={value}
				name={name}
				value={value}
				checked={checked}
				onChange={onChange}
				className="mr-2 gray-400 font-14"
			/>
			<label
				htmlFor={value}
				className="weight-500 font-14 text-gray-400"
			>
				{label}
			</label>
		</div>
	);
};

export { TextField, RadioField };
