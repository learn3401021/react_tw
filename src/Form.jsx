import { useState } from "react";
import { TextField } from "./Utils";

const CreateJobForm = () => {
	const [form, setForm] = useState({
		jobTitle: "",
		companyName: "",
		industry: "",
		location: "",
		remoteType: "",
	});

	const handleInputChange = (event) => {
		const { name, value } = event.target;
		setForm((prevForm) => ({
			...prevForm,
			[name]: value,
		}));
	};

	const handleSubmit = (event) => {
		event.preventDefault();
		// Perform form submission or validation logic here
	};

	return (
		<div className="container mx-auto p-4 ">
			<div className="flex justify-between mb-4">
				<h1 className="xl:text-xl font-normal text-20  leading-28 tracking-normal text-left">Create a job</h1>
				<span className="text-base font-medium leading-6 tracking-normal text-right">Step 1</span>
			</div>
			<form onSubmit={handleSubmit}>
				<div className="h-96 overflow-y-auto">
					<TextField
						label="Job title"
						name="jobTitle"
						value={form.jobTitle}
						onChange={handleInputChange}
						required
						placeholder="ex. UX UI Designer"
					/>
					<TextField
						label="Company name"
						name="companyName"
						value={form.companyName}
						onChange={handleInputChange}
						required
						placeholder="ex. Google"
					/>
					<TextField
						label="Industry"
						name="industry"
						value={form.industry}
						onChange={handleInputChange}
						required
						placeholder="ex. Information Technology"
					/>
					<div className="flex mb-4">
						<div className="w-1/2 mr-2">
							<TextField
								label="Location"
								name="location"
								value={form.location}
								onChange={handleInputChange}
								placeholder="ex. Chennai"
							/>
						</div>
						<div className="w-1/2 ml-2">
							<TextField
								label="Remote type"
								name="remoteType"
								value={form.remoteType}
								onChange={handleInputChange}
								placeholder="ex. In-office"
							/>
						</div>
					</div>
				</div>
				<div className="flex justify-end">
					<button
						type="submit"
						className="bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600"
					>
						Next
					</button>
				</div>
			</form>
		</div>
	);
};

export default CreateJobForm;
