const Card = ({ logo, jobTitle, companyName, location, partTimeTiming, experience, salary, numOfEmployees, isExternal = false }) => {
	return (
		<div className="bg-white shadow-md rounded-md p-6 w-600 m-4 flex flex-row">
			<div>
				<div className="w-12 h-12 rounded-md overflow-hidden mt-2">
					<img
						src={logo}
						alt="Company Logo"
						className="w-full h-full object-cover"
					/>
				</div>
			</div>
			<div>
				<div className="flex items-start">
					<div className="ml-4">
						<h2 className="text-xl font-semibold">{jobTitle}</h2>
						<p className="text-16 font-medium">{companyName}</p>
						<p className="text-gray-500">{location}</p>
					</div>
				</div>
				<div className="mt-4 ml-4">
					<p className="font-normal">Part-time Timing {partTimeTiming}</p>
					<p className="font-normal">Experience {experience}</p>
					<p className="font-normal">Salary (₹) {salary}</p>
					<p className="font-normal">Number of Employees {numOfEmployees}</p>
				</div>
				<div className="mt-6 ml-4">
					{isExternal ? (
						<button className="bg-white-500 text-blue-500 px-4 py-2 rounded-md mr-4 border border-blue-500"> External Apply </button>
					) : (
						<button className="bg-blue-500 text-white px-4 py-2 rounded-md">Apply Now</button>
					)}
				</div>
			</div>
		</div>
	);
};

let companyLogo = "https://play-lh.googleusercontent.com/TBRwjS_qfJCSj1m7zZB93FnpJM5fSpMA_wUlFDLxWAb45T9RmwBvQd5cWR5viJJOhkI";

const App = () => {
	let data = [
		{
			id: 1,
			logo: companyLogo,
			jobTitle: "UX UI Designer",
			companyName: "Great Vibes - Information Technology",
			location: "Chennai, Tamilnadu, India (In-office)",
			experience: "2+ years",
			salary: "70,000 - 90,000 per month",
			numOfEmployees: "51-200 employees",
			partTimeTiming: "(9.00 am - 5.00 pm IST)",
		},
		{
			id: 2,
			logo: companyLogo,
			jobTitle: "Developer",
			companyName: "Great Vibes - Information Technology",
			location: "Chennai, Tamilnadu, India (In-office)",
			experience: "2+ years",
			salary: "70,000 - 90,000 per month",
			numOfEmployees: "51-200 employees",
			partTimeTiming: "(9.00 am - 5.00 pm IST)",
		},
		{
			id: 3,
			logo: companyLogo,
			jobTitle: "React Developer",
			companyName: "Great Vibes - Information Technology",
			location: "Chennai, Tamilnadu, India (In-office)",
			experience: "2+ years",
			salary: "70,000 - 90,000 per month",
			numOfEmployees: "51-200 employees",
			partTimeTiming: "(9.00 am - 5.00 pm IST)",
			isExternal: true,
		},
	];
	return (
		<div className="container py-8 flex flex-wrap content-around items-center">
			{data.map((item) => (
				<Card
					key={item.id}
					logo={item.logo}
					jobTitle={item.jobTitle}
					companyName={item.companyName}
					location={item.location}
					experience={item.experience}
					salary={item.salary}
					numOfEmployees={item.numOfEmployees}
					partTimeTiming={item.partTimeTiming}
					isExternal={item.isExternal}
				/>
			))}
		</div>
	);
};

export default App;
