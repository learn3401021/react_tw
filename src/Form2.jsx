import { useState } from "react";
import { TextField, RadioField } from "./Utils";

const Step2Form = () => {
	const [form, setForm] = useState({
		expMin: "",
		expMax: "",
		salaryMin: "",
		salaryMax: "",
		totalEmployees: "",
		applyType: "quickApply",
	});

	const handleInputChange = (event) => {
		const { name, value } = event.target;
		setForm((prevForm) => ({
			...prevForm,
			[name]: value,
		}));
	};

	const handleApplyTypeChange = (event) => {
		const { value } = event.target;
		setForm((prevForm) => ({
			...prevForm,
			applyType: value,
		}));
	};

	const handleSubmit = (event) => {
		event.preventDefault();
		// Perform form submission or validation logic here
	};

	return (
		<div className="container mx-auto p-4">
			<div className="flex justify-between mb-4">
				<h1 className="xl:text-xl font-normal text-20 leading-28 tracking-normal text-left">Create a job</h1>
				<span className="text-base font-medium leading-6 tracking-normal text-right">Step 2</span>
			</div>

			<form onSubmit={handleSubmit}>
				<div className="h-96 overflow-y-auto">
					<div className="flex">
						<div className="w-1/2 mr-2">
							<TextField
								label="Experience"
								name="expMin"
								value={form.expMin}
								onChange={handleInputChange}
								placeholder="Minimum"
							/>
						</div>
						<div className="w-1/2 ml-2 flex items-end">
							<TextField
								label=""
								name="expMax"
								value={form.expMax}
								onChange={handleInputChange}
								placeholder="Maximum"
							/>
						</div>
					</div>

					<div className="flex">
						<div className="w-1/2 mr-2">
							<TextField
								label="Salary"
								name="salaryMin"
								value={form.salaryMin}
								onChange={handleInputChange}
								placeholder="Minimum"
							/>
						</div>
						<div className="w-1/2 ml-2 flex items-end">
							<TextField
								label=""
								name="salaryMax"
								value={form.salaryMax}
								onChange={handleInputChange}
								placeholder="Maximum"
							/>
						</div>
					</div>

					<TextField
						label="Total employee"
						name="totalEmployees"
						value={form.totalEmployees}
						onChange={handleInputChange}
						placeholder="ex. 100"
					/>

					<label className="block weight-500">Apply type</label>
					<div className="flex mb-4">
						<div className="flex items-center">
							<RadioField
								label="Quick apply"
								name="applyType"
								value="quickApply"
								checked={form.applyType === "quickApply"}
								onChange={handleApplyTypeChange}
							/>
							<RadioField
								label="External apply"
								name="applyType"
								value="externalApply"
								checked={form.applyType === "externalApply"}
								onChange={handleApplyTypeChange}
							/>
						</div>
					</div>
				</div>
				<div className="flex justify-end">
					<button
						type="submit"
						className="bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600"
					>
						Save
					</button>
				</div>
			</form>
		</div>
	);
};

export default Step2Form;
